public class Products {
    private String proizvod;
    private double cost;
    private String name;
    public Products(String proizvod, double cost, String name){
        this.proizvod = proizvod;
        this.cost = cost;
        this.name = name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    public void setProizvod(String proizvod) {
        this.proizvod = proizvod;
    }
    public String getProizvod() {
        return proizvod;
    }
    public String getName() {
        return name;
    }
    public double getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return String.format("Производитель: %s\n Название: %s\nЦена: %f \n",proizvod,name,cost);
    }
}
