import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Products> prod = new ArrayList<>();
        Scanner scan = new Scanner(System.in);
        int k = 0;
        while (true) {
            System.out.println("1 - Добавление товара \n2 - Редактирование товара \n3 - Просмотр товара");
            String input = scan.next();
            switch (input) {
                case "1":
                    System.out.println("Добавление товара");
                    System.out.println("Выберите тип товара. 1 - видеокарты, 2 - мониторы");
                    k = scan.nextInt();
                    switch (k) {
                        case 1:
                            prod.add(new VideoCards(typeValue("Введите производителя"), typeValue("Введите название"), Double.valueOf(typeValue("Введите цену")), Integer.valueOf(typeValue("Введите объем видеопамяти")), Float.valueOf(typeValue("Введите частоту"))));
                            break;
                        case 2:
                            prod.add(new Monitors(typeValue("Введите производителя"), typeValue("Введите наименование"), Double.valueOf(typeValue("Введите цену")), Double.valueOf(typeValue("Введите диагональ в дюймах")), Float.valueOf(typeValue("Введите частоту")), typeValue("Введите тип матрицы"), new int[]{Integer.valueOf(typeValue("Введите ширину")), Integer.valueOf(typeValue("Введите высоту"))}));

                    }
                    break;
                case "2":
                    System.out.println("Редактирование товара");
                    int id = scan.nextInt();
                    for (int i = 0; i < prod.size(); i++) {
                        if (id == i) {
                            if (prod.get(id) instanceof VideoCards) {
                                System.out.println(prod.get(i));
                                System.out.println("Выберите, что редактировать:\n1 - производитель\n2 - название\n3 - цена \n 4 - видеопамять\n5 - частота\n6 - все\n= - назад");
                                String vib = scan.next();
                                while (vib != "=") {
                                    switch (vib) {
                                        case "1":
                                            prod.get(id).setProizvod(typeValue("Введите нового производителя"));
                                            break;
                                        case "2":
                                            prod.get(id).setName(typeValue("Введите новое название"));
                                            break;
                                        case "3":
                                            prod.get(id).setCost(Double.valueOf(typeValue("Введите новую цену")));
                                            break;
                                        case "4":
                                            ((VideoCards) prod.get(id)).setVideoMemory(Integer.valueOf(typeValue("Введите новую видеопамять")));
                                            break;
                                        case "5":
                                            ((VideoCards) prod.get(id)).setFrequency(Float.valueOf(typeValue("Введите новую частоту")));
                                            break;
                                        case "6":
                                            prod.set(id, new VideoCards(typeValue("Введите производителя"), typeValue("Введите название"), Double.valueOf(typeValue("Введите цену")), Integer.valueOf(typeValue("Введите объем видеопамяти")), Float.valueOf(typeValue("Введите частоту"))));
                                            break;
                                        default:
                                            break;
                                    }
                                    vib = scan.next();
                                }
                            } else if (prod.get(id) instanceof Monitors) {
                                    System.out.println(prod.get(i));
                                    System.out.println("Выберите, что редактировать:\n1 - производитель\n2 - название\n3 - цена \n 4 - диагональ\n5 - частота\n6 - разрешение экрана\n7 - тип матрицы\n8 - все\n= - назад");
                                    String vib = scan.next();
                                    while (!vib.equals("=")) {
                                        vib = scan.next();
                                        switch (vib) {
                                            case "1":
                                                prod.get(id).setProizvod(typeValue("Введите нового произодителя"));
                                                break;
                                            case "2":
                                                prod.get(id).setName(typeValue("Введите новое название"));
                                                break;
                                            case "3":
                                                prod.get(id).setCost(Double.valueOf(typeValue("Введите новую цену")));
                                                break;
                                            case "4":
                                                ((Monitors)prod.get(id)).setDiag(Double.valueOf(typeValue("Введите новую диагональ")));
                                                break;
                                            case "5":
                                                ((Monitors)prod.get(id)).setFrequency(Float.valueOf(typeValue("Введите новую частоту")));
                                                break;
                                            case "6":
                                                ((Monitors)prod.get(id)).setResolution(new int[]{Integer.valueOf(typeValue("Введите новую ширину")), Integer.valueOf(typeValue("Введите новую высоту"))});
                                            case "7":
                                                ((Monitors)prod.get(id)).setType(typeValue("Введите новый тип матрицы"));
                                            case "8":
                                                prod.set(id, new Monitors(typeValue("Введите нового производителя"), typeValue("Введите новое наименование"), Double.valueOf(typeValue("Введите новую цену")), Double.valueOf(typeValue("Введите новую диагональ в дюймах")), Float.valueOf(typeValue("Введите новую частоту")), typeValue("Введите новый тип матрицы"), new int[]{Integer.valueOf(typeValue("Введите новую ширину")), Integer.valueOf(typeValue("Введите новую высоту"))}));
                                            default:
                                                break;
                                        }
                                        vib = scan.next();
                                    }
                                }
                            }
                        }
                        break;
                        case "3":
                            System.out.println("Просмотр товара");
                           for(Products a:prod) {
                                        if(a instanceof VideoCards) {
                                            System.out.println(a);
                                        }
                                        if (a instanceof Monitors) {
                                            System.out.println(a);
                                        }
                                    }
                           break;
                    }
            }
        }
    public static String typeValue(String type){
        Scanner scan = new Scanner(System.in);
        System.out.println(type);
        return scan.next();
    }
}
