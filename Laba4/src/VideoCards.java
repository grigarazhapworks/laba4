public class VideoCards extends Products {
    private int videoMemory;
    private float frequency;

    public VideoCards(String proizvod, String name, double cost, int videoMemory, float frequency){
        super(proizvod,cost,name);
        this.videoMemory = videoMemory;
        this.frequency = frequency;
    }

    public void setProizvod(String proizvod) {
        super.setProizvod(proizvod);
    }

    public void setVideoMemory(int videoMemory) {
        this.videoMemory = videoMemory;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    public void setName(String name) {
        super.setName(name);
    }

    public void setCost(double cost) {
        super.setCost(cost);
    }

    public String getProizvod() {
        return super.getProizvod();
    }

    public int getVideoMemory() {
        return videoMemory;
    }

    public float getFrequency() {
        return frequency;
    }

    public String getName() {
        return super.getName();
    }

    public double getCost() {
        return super.getCost();
    }

    @Override
    public String toString() {
        return String.format("Производитель: %s\n Название: %s \n " +
                "Цена: %f \n Видеопамять: %d \n Частота: %f", getProizvod(), getName(), getCost(), videoMemory, frequency);
    }
}