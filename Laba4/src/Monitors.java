public class Monitors extends Products
{
    private double diag;
    private float frequency;
    private String type;
    private int[] resolution;

    public Monitors(String proizvod, String name, double cost, double diag, float frequency,String type,int[] resolution){
        super(proizvod, cost, name);
        this.diag = diag;
        this.frequency = frequency;
        this.type = type;
        this.resolution = resolution;
    }

    public void setProizvod(String proizvod) {
        super.setProizvod(proizvod);
    }

    public void setDiag(double diag) {
        this.diag = diag;
    }
    public void setResolution(int[] resolution){
        this.resolution = resolution;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    public void setName(String name) {
        super.setName(name);
    }

    public void setCost(double cost) {
        super.setCost(cost);
    }
    public String setType(String type){
        return type;
    }

    public String getProizvod() {
        return super.getProizvod();
    }

    public double getDiag() {
        return diag;
    }

    public float getFrequency() {
        return frequency;
    }

    public String getName() {
        return super.getName();
    }

    public double getCost() {
        return super.getCost();
    }
    public String getType(){
        return type;
    }
    public int[] getResolution(){
        return resolution;
    }

    @Override
    public String toString() {
        return String.format("Производитель: %s\n Название: %s \n " +
                "Цена: %f \n Диагональ: %f дюймов\n Частота: %f Гц\n Разрешение: %d x %d\n Тип матрицы: %s" , getProizvod(), getName(), getCost(), diag, frequency,resolution[0],resolution[1],type);
    }
}
